# README Using snippets in sublime #

Tell sublime about our bitbucket repo with the snippets

* SHIFT + CTRL + P
* Search for: Package Control: Add repository
* Add the link to bitbucket repo with snips https://AngusGalloway@bitbucket.org/jema41x/sublime-snippets.git
* SHIFT + CTRL + P 
* Search for: Package Control: Install Package
* Type 'sublime-snippets', note the repo which is on bitbucket, click that one. 
* In a terminal, type 'touch sample.tex'
* Still in terminal, type 'sublime sample.tex', then in the editor type 'article', then and then press tab, which results in:

\documentclass[a4paper,12pt]{article}
\begin{document}

\section{Heading 1}

\subsection{Heading 2}

\end{document}

* Start typing and 'Heading 1' will be replaced with your own heading. 
* Press tab and start typing, 'Heading 2' will be replaced. 
* For detail on how this works, see USEFUL LINKS and src below.

This is the source for the snippet you just used, for creating a generic LaTeX article skeleton:

<snippet>
	<content><![CDATA[
\documentclass[a4paper,12pt]{article}
\begin{document}

\section{${1:Heading 1}}

\subsection{${2:Heading 2}}

\end{document}
]]></content>
	<!-- Optional: Set a tabTrigger to define how to trigger the snippet -->
	<tabTrigger>article</tabTrigger> -->
	<!-- Optional: Set a scope to limit where the snippet will trigger -->
	<scope>text.tex</scope> -->
</snippet>

### USEFUL LINKS ###
1. http://mandymadethis.com/sharing-sublime-text-snippets/
2. http://stackoverflow.com/questions/13922174/defining-scope-for-custom-sublime-text-2-snippets/#answer-14682280

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Other Snippets ###

* sec - H1 section with label
* bsec - Brief H1 section, w/o label
* ben - begin enumerated list
* tex-pkg - header with common packages, choose doc fmt